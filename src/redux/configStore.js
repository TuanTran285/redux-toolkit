import {configureStore} from '@reduxjs/toolkit'
import shopReducer from './reducers/shopReducer'
export const store =  configureStore({
    reducer: {
        shopReducer
    }
})


// khi cài reudx-toolkit thì nó có sẵn redux-thunk với redux dev-tool