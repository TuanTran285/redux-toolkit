import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'

const initialState = {
    cart: [
        {
            id: 1, name: 'product1', image: "https://www.shutterstock.com/shutterstock/photos/1777161830/display_1500/stock-photo-zhytomyr-ukraine-june-nike-air-force-sage-white-sneakers-product-shot-on-gray-1777161830.jpg", price: 1000, quantity: 1
        },
        {
            id: 2, name: 'product1', image: "https://www.shutterstock.com/shutterstock/photos/1777161830/display_1500/stock-photo-zhytomyr-ukraine-june-nike-air-force-sage-white-sneakers-product-shot-on-gray-1777161830.jpg", price: 1000, quantity: 1
        },
    ],
    dataProduct: [
        {
            id: 3, name: 'product1', image: "https://www.shutterstock.com/shutterstock/photos/1777161830/display_1500/stock-photo-zhytomyr-ukraine-june-nike-air-force-sage-white-sneakers-product-shot-on-gray-1777161830.jpg", price: 1000, quantity: 1
        },
    ]
}

const shopReducer = createSlice({
    name: 'shopReducer', //tên reducer
    initialState, //giá trị mặc định của reducer (stateDefault)
    reducers: {
        getProductApi: (state, action) => {
            state.dataProduct = action.payload
        },
        addToCart: (state, action) => {
            // redux toolkit thì không cần clone và return => chỉ cần code logic là được
            state.cart.push(action.payload)
        },
        deleteItem: (state, action) => {
            let id = action.payload
            state.cart = state.cart.filter(item => item.id !== id)
        }
    }
})

export const { getProductApi, addToCart, deleteItem } = shopReducer.actions

export default shopReducer.reducer

// gọi api trong đây l uôn

// -----------action-thunk-----------
export const getAllProduct = () => {
    return async (dispatch, getState) => {
        try {
            const result = await axios({
                url: "https://api.storerestapi.com/products",
                method: "GET"
            })
            // xử lý dispatch lên reducer
            // dispatch({
            //     type: "shopReducer/getProductApi",
            //     data: result.data.data
            // })
            dispatch(getProductApi(result.data.data))
            // nó tự tạo ra  {
            // type: shopReducer/getProductApi,
            // payload: result.data.data
            // }
        } catch (err) {
            console.log("err", err)
        }
    }
}
