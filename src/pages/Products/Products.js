import React, { useEffect } from 'react'
import Carts from './Carts'
import ProductItem from './ProductItem'
import {useSelector, useDispatch} from 'react-redux'
import { getAllProduct } from '../../redux/reducers/shopReducer'
export default function Products() {
    const {dataProduct} = useSelector((state) => state.shopReducer)
    const dispatch = useDispatch()
    useEffect(() => {
        const actionThunk = getAllProduct();
        dispatch(actionThunk)
    }, [])
    return (
        <div className='container'>
            <h3 className='text-center'>Shoes shop</h3>
            <Carts />
            <div className='row my-5'>
                {dataProduct.map((item, index) => {
                    return <ProductItem key={index} item={item}/>
                })}
            </div>
        </div>
    )
}
