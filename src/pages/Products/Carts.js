import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { deleteItem } from '../../redux/reducers/shopReducer'
export default function Carts() {
    const { cart } = useSelector(state => state.shopReducer)
    const dispatch = useDispatch()
    return (
        <div>
            <h3>Carts</h3>
            <table className='table'>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {cart.map((item, index) => {
                        return (
                            <tr key={index}>
                            <td>{item.id}</td>
                            <td>{item.name}</td>
                            <td><img style={{ width: 50, height: 50, borderRadius: 6, objectFit: "cover" }}
                                src={item.image} alt="Lỗi hình ảnh" /></td>
                            <td>{item.price}</td>
                            <td>{item.quantity}</td>
                            <td>{item.price * item.quantity}</td>
                            <td><button onClick={() => {
                                dispatch(deleteItem(item.id))
                            }} className='btn btn-danger'>Xóa</button></td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
