import React from 'react'
import {useDispatch} from 'react-redux'
import { addToCart } from '../../redux/reducers/shopReducer'
export default function ProductItem({item}) {
    const dispatch = useDispatch()
    return (
        <div className="card col-3">
            <img className="card-img-top" alt="Lỗi image" src={item.image} style={{objectFit: "cover", height: "auto"}} />
            <div className="card-body">
                <h4 className="card-title">Tên: {item.name}</h4>
                <p className="card-text">Giá: {item.price}</p>
                <button onClick={() => {
                    let newItem = {...item, id: Math.random(),quantity: 1}
                    dispatch(addToCart(newItem))
                }} className='btn btn-success'>Add to cart</button>
            </div>
        </div>
    )
}
